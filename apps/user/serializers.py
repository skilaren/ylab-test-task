from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:

        model = User
        fields = ["id", "email", "password", "balance", "balance_currency"]
        extra_kwargs = {"password": {"write_only": True}}

        read_only_fields = ("id",)
