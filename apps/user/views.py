from django.db.models import Q
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from apps.transaction.models import Transaction
from apps.transaction.serializers import TransactionSerializer
from apps.user.models import User
from rest_framework import viewsets, status
from apps.user.serializers import UserSerializer


class UserViewSet(viewsets.GenericViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(
        detail=True,
        methods=["get"],
        url_path="transactions",
        url_name="transactions",
        permission_classes=[IsAdminUser],
    )
    def transactions_for_admin(self, request, pk=None):
        user = self.get_object()
        if not request.user.is_admin and not request.user == user:
            return Response(
                data="Unauthorized access", status=status.HTTP_401_UNAUTHORIZED
            )
        transactions = Transaction.objects.filter(Q(sender=user) | Q(receiver=user))
        transactions_response = TransactionSerializer(transactions, many=True)
        return Response(data=transactions_response.data, status=status.HTTP_200_OK)

    @action(
        detail=False,
        methods=["get"],
        url_path="transactions",
        url_name="transactions",
        permission_classes=[IsAuthenticated],
    )
    def transactions_for_user(self, request):
        transactions = Transaction.objects.filter(
            Q(sender=request.user) | Q(receiver=request.user)
        )
        transactions_response = TransactionSerializer(transactions, many=True)
        return Response(data=transactions_response.data, status=status.HTTP_200_OK)
