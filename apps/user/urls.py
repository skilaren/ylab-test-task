from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.user.views import UserViewSet


user_router = DefaultRouter()
user_router.register("user", UserViewSet, basename="user")


urlpatterns = [
    path("", include("djoser.urls")),
    path("", include(user_router.urls)),
    path("auth/", include("djoser.urls.authtoken")),
]
