from django.test import TestCase
from djmoney.money import Money

from apps.user.models import User


class UserModelTest(TestCase):
    def setUp(self):
        User.objects.create(
            email="test_user@mail.ru",
            password="StRoNgPass123",
            balance=Money("100", "USD"),
        )

    def test_user_creation(self):
        user = User.objects.get(email="test_user@mail.ru")
        money = Money("100", "USD")
        self.assertIsNotNone(user)
        self.assertEqual("test_user@mail.ru", user.email)
        self.assertEqual(user.balance, money)
