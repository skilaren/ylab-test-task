from django.urls import reverse
from djmoney.money import Money
from rest_framework import status
from rest_framework.test import APITestCase
from djoser.conf import settings as djoser_settings

from apps.transaction.models import Transaction
from apps.user.models import User

Token = djoser_settings.TOKEN_MODEL


class UserApiTest(APITestCase):
    def test_user_creation(self):
        url = reverse("user-list")
        email = "maksimus@mail.ru"
        data = {
            "email": email,
            "password": "StRoNgPaSS123",
            "balance": "0.0000",
            "balance_currency": "USD",
        }

        response = self.client.post(url, data, format("json"))
        money = Money("0.0000", "USD")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().email, email)
        self.assertEqual(User.objects.get().balance, money)

    def test_user_info(self):
        user = User.objects.create(
            email="test_user@mail.ru",
            password="StRoNgPass123",
            balance=Money("100", "USD"),
        )
        self.base_url = reverse("user-me")

        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.get(self.base_url)

        self.assertEqual(response.data["balance"], "100.0000")
        self.assertEqual(response.data["balance_currency"], "USD")

    def test_get_transactions_of_user(self):
        user1 = User.objects.create(
            email="test_user1@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )
        user2 = User.objects.create(
            email="test_user2@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )
        url = reverse("user-transactions")
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("50", "USD")
        )
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("10", "USD")
        )
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("30", "USD")
        )

        token = Token.objects.create(user=user1)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_get_transactions_of_user_by_not_admin(self):
        user1 = User.objects.create(
            email="test_user1@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )
        user2 = User.objects.create(
            email="test_user2@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )
        url = reverse("user-transactions", kwargs={"pk": user2.id},)
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("50", "USD")
        )
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("10", "USD")
        )
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("30", "USD")
        )

        token = Token.objects.create(user=user1)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_transactions_of_user_by_admin(self):
        user1 = User.objects.create(
            email="test_user1@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )
        user2 = User.objects.create(
            email="test_user2@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )
        admin_user = User.objects.create_superuser(
            email="test_admin_user@mail.ru",
            password="StRoNgPass123",
            balance=Money("200", "USD"),
        )

        url = reverse("user-transactions", kwargs={"pk": user2.id},)
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("50", "USD")
        )
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("10", "USD")
        )
        Transaction.objects.create(
            sender=user1, receiver=user2, amount=Money("30", "USD")
        )

        token = Token.objects.create(user=admin_user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
