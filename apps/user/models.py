from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from djmoney.models.fields import MoneyField
from django.utils.translation import gettext as _
from djmoney.contrib.exchange.models import convert_money


class UserManager(BaseUserManager):
    def create_user(self, email, password, balance=None):
        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(email=self.normalize_email(email), balance=balance)

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, balance=None):
        user = self.create_user(
            email=self.normalize_email(email), password=password, balance=balance
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):

    email = models.EmailField(unique=True, verbose_name=_("E-mail address"))
    balance = MoneyField(
        max_digits=19, decimal_places=4, null=False, verbose_name="Balance",
    )
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["balance"]

    def decrease_balance(self, amount):
        amount_in_user_currency = convert_money(amount, self.balance.currency)
        self.balance = self.balance - amount_in_user_currency
        self.save()

    def increase_balance(self, amount):
        amount_in_user_currency = convert_money(amount, self.balance.currency)
        self.balance = self.balance + amount_in_user_currency
        self.save()

    def is_staff(self):
        return self.is_admin
