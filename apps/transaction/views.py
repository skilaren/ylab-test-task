from rest_framework.permissions import IsAuthenticated

from apps.transaction.models import Transaction
from rest_framework import viewsets, mixins
from apps.transaction.serializers import TransactionSerializer


class TransactionViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        request.data["sender"] = request.user.id
        if not request.data.get("amount_currency"):
            request.data["amount_currency"] = request.user.balance.currency
        return super(TransactionViewSet, self).create(request, *args, **kwargs)
