from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.transaction.models import Transaction

User = get_user_model()


class TransactionSerializer(serializers.ModelSerializer):
    def validate(self, data):
        """
        Check that sender is not the same as receiver
        :param data:
        :return: validated data
        """
        if data["sender"] == data["receiver"]:
            raise serializers.ValidationError("Sender is the same as receiver")
        return data

    class Meta:

        model = Transaction
        fields = ["id", "sender", "receiver", "amount", "amount_currency"]

        read_only_fields = ("id",)
