from django.contrib.auth import get_user_model
from django.db import models, transaction
from django.utils.translation import gettext as _
from djmoney.models.fields import MoneyField

User = get_user_model()


class TransactionManager(models.Manager):
    def create(self, **kwargs):

        new_transaction = self.model(**kwargs)
        new_transaction.execute()
        new_transaction.save()

        return new_transaction


class Transaction(models.Model):

    sender = models.ForeignKey(
        User,
        verbose_name=_("Who sent"),
        related_name=_("sent_transactions"),
        on_delete=models.DO_NOTHING,
    )
    receiver = models.ForeignKey(
        User,
        verbose_name=_("Who got"),
        related_name=_("received_transactions"),
        on_delete=models.DO_NOTHING,
    )
    amount = MoneyField(
        max_digits=19, decimal_places=4, null=False, verbose_name="Amount",
    )

    objects = TransactionManager()

    def execute(self):
        with transaction.atomic():
            self.sender.decrease_balance(self.amount)
            self.receiver.increase_balance(self.amount)
