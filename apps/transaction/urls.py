from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.transaction.views import TransactionViewSet

transaction_router = DefaultRouter()
transaction_router.register("send", TransactionViewSet, basename="transaction")

urlpatterns = [
    path("", include(transaction_router.urls)),
]
