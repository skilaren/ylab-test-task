from django.test import TestCase
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money
from djmoney.contrib.exchange.backends import OpenExchangeRatesBackend

from apps.transaction.models import Transaction
from apps.user.models import User


class UserModelTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(
            email="test_user@mail.ru",
            password="StRoNgPass123",
            balance=Money("100", "USD"),
        )
        self.user2 = User.objects.create(
            email="test_user2@mail.ru",
            password="StRoNgPass123",
            balance=Money("350", "USD"),
        )

    def test_transaction_creation(self):
        t = Transaction.objects.create(
            sender=self.user1, receiver=self.user2, amount=Money("50", "USD")
        )
        user = User.objects.get(email="test_user@mail.ru")
        money = Money("50", "USD")
        self.assertIsNotNone(user)
        self.assertEqual("test_user@mail.ru", user.email)
        self.assertEqual(user.balance, money)

    def test_transaction_creation_different_currency(self):
        OpenExchangeRatesBackend().update_rates()
        t = Transaction.objects.create(
            sender=self.user1, receiver=self.user2, amount=Money("50", "EUR")
        )
        user = User.objects.get(email="test_user@mail.ru")
        money = Money("100", "USD", decimal_places=4) - convert_money(
            Money("50", "EUR"), "USD"
        )
        self.assertIsNotNone(user)
        self.assertEqual("test_user@mail.ru", user.email)
        self.assertAlmostEqual(user.balance.amount, money.amount, places=3)
