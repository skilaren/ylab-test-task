from django.urls import reverse
from djmoney.contrib.exchange.backends import OpenExchangeRatesBackend
from djmoney.money import Money
from rest_framework import status
from rest_framework.test import APITestCase
from djoser.conf import settings as djoser_settings
from djmoney.contrib.exchange.models import convert_money

from apps.transaction.models import Transaction
from apps.user.models import User

Token = djoser_settings.TOKEN_MODEL


class UserApiTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create(
            email="test_user@mail.ru",
            password="StRoNgPass123",
            balance=Money("100", "USD"),
        )
        self.user2 = User.objects.create(
            email="test_user2@mail.ru",
            password="StRoNgPass123",
            balance=Money("350", "USD"),
        )
        self.user3 = User.objects.create(
            email="test_user3@mail.ru",
            password="StRoNgPass123",
            balance=Money("500", "USD"),
        )
        OpenExchangeRatesBackend().update_rates()

    def test_transaction_sending(self):
        url = reverse("transaction-list")
        data = {"receiver": self.user3.id, "amount": "75"}

        token = Token.objects.create(user=self.user1)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.post(url, data, format("json"))
        self.user1 = User.objects.get(id=self.user1.id)
        self.user3 = User.objects.get(id=self.user3.id)
        money1 = Money("25", "USD")
        money3 = Money("575", "USD")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(self.user1.balance, money1)
        self.assertEqual(self.user3.balance, money3)

    def test_transaction_sending_with_btc(self):
        url = reverse("transaction-list")
        user4 = User.objects.create(
            email="test_user4@mail.ru",
            password="StRoNgPass123",
            balance=Money("10", "BTC"),
        )

        data = {"receiver": self.user3.id, "amount": "1"}

        token = Token.objects.create(user=user4)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.post(url, data, format("json"))
        self.user4 = User.objects.get(id=user4.id)
        self.user3 = User.objects.get(id=self.user3.id)
        money1 = Money("9", "BTC")
        money3 = Money("500", "USD") + convert_money(Money("1", "BTC"), "USD")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(self.user4.balance, money1)
        self.assertAlmostEqual(self.user3.balance.amount, money3.amount, places=3)

    def test_transaction_sending_with_another_sender(self):
        """
        User1 authorized and transaction made with User2 as sender and User3 as receiver
        Expected: User2 replaced with User1 and User2 balance is not changed
        :return:
        """
        url = reverse("transaction-list")
        data = {
            "sender": self.user2.id,
            "receiver": self.user3.id,
            "amount": "75",
            "amount_currency": "USD",
        }

        token = Token.objects.create(user=self.user1)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.post(url, data, format("json"))

        self.user1 = User.objects.get(id=self.user1.id)
        old_user_2 = self.user2
        self.user2 = User.objects.get(id=self.user2.id)
        self.user3 = User.objects.get(id=self.user3.id)
        money1 = Money("25", "USD")
        money3 = Money("575", "USD")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(self.user1.balance, money1)
        self.assertEqual(self.user3.balance, money3)
        self.assertEqual(old_user_2.balance, self.user2.balance)

    def test_transaction_sending_different_currency(self):
        OpenExchangeRatesBackend().update_rates()
        url = reverse("transaction-list")
        data = {"receiver": self.user2.id, "amount": "50", "amount_currency": "EUR"}

        token = Token.objects.create(user=self.user1)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = self.client.post(url, data, format("json"))
        self.user1 = User.objects.get(id=self.user1.id)
        money = Money("100", "USD", decimal_places=4) - convert_money(
            Money("50", "EUR"), "USD"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertAlmostEqual(self.user1.balance.amount, money.amount, places=3)
