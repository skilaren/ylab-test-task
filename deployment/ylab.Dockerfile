FROM python:3.8-alpine3.10

RUN apk update && apk upgrade && apk add bash
RUN apk add gcc linux-headers musl-dev libffi-dev openssl-dev g++
RUN apk add postgresql-dev
COPY . ./app

RUN pip install --no-cache -r ./app/requirements.txt
EXPOSE 8000/tcp
