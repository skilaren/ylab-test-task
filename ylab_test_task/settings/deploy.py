from .base import *
from .currencies import *

if DEBUG:
    # Database
    # https://docs.djangoproject.com/en/3.0/ref/settings/#databases

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": "testtaskdb",
            "USER": "testtask",
            "PASSWORD": "testtask",
            "HOST": "db",
            "PORT": "5432",
        }
    }
