import moneyed
from moneyed.localization import _FORMATTER
from decimal import ROUND_HALF_EVEN

BTC = moneyed.add_currency(
    code="BTC", numeric="888", name="Bitcoin", countries=["World"]
)

_FORMATTER.add_sign_definition("default", BTC, prefix=u"BTC ")

_FORMATTER.add_formatting_definition(
    "en_US",
    group_size=3,
    group_separator=".",
    decimal_point=",",
    positive_sign="",
    trailing_positive_sign="",
    negative_sign="-",
    trailing_negative_sign="",
    rounding_method=ROUND_HALF_EVEN,
)

CURRENCIES = ["USD", "RUB", "EUR", "GBP", "BTC"]
