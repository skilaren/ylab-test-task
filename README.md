[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
![Code coverage: 99%](https://img.shields.io/badge/coverage-99%25-brightgreen)
![Python version: 3.8](https://img.shields.io/badge/Python-3.8-orange)
![Django version: 3.0.1](https://img.shields.io/badge/Django-3.0.1-blue)

## YLab test task: Money Transfer mini-app

To run the server you have two options:

- Run docker-compose on deployment folder
- Run docker images from repository

#### docker-compose

```
    docker-compose -f deployment/ylab_test_task.yml up -d
```

#### docker images
    docker network create ylabnetwork
    docker run --name db -p 5432:5432 --env-file /path/to/envfile --network ylabnetwork postgres:latest
    docker run --name server -p 8000:8000 --network ylabnetwork -it skilaren/ylabserver:latest /bin/bash
    
And run this command in container bash
    
    python ./app/manage.py migrate && python ./app/manage.py update_rates && python ./app/manage.py runserver 0.0.0.0:8000
    
Now server should be available at http://localhost:8000
    
## Usage

**Create user:**

POST request to ```http://host:port/users/```
with body
```
{
    "balance": 0,
    "balance_currency": "USD",
    "email": "test@mail.ru",
    "password": "somepassword"
}
```

**Login**

POST request to ```http://host:port/auth/token/login``` with body
```
{
    "email": "test@mail.ru",
    "password": "somepassword"
}
```

Request returns token for further requests

**Get user info**

GET request to ```http://host:port/users/me``` after authentication

**Send transaction**

POST request to ```http://host:port/send``` after authentication with body
```
{
    "receiver": id_of_user_who_you_want_to_send_money,
    "amount": 50,
    "amount_currency": "USD" // optional, default value is user's balance currency
}
```

**Get your transactions**

GET request to ```http://host:port/user/transactions``` after authentication

**Get user transactions for admin**

GET request to ```http://host:port/user/<user_id>/transactions``` after authentication


## Implementation (used packages)
    
For REST API implementation: **Django** with **djangorestframework**

For user authorization: **djoser**

For money and money exchange handling: **django-money**

For code-styling: **Black** and **flake8**

For testing: **coverage** 